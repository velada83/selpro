package testcases;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
//import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CreateLead {

	@Test(dataProvider="paneer")
	//public static void main(String[] args) throws InterruptedException 
	public void run(String name,String fname,int num)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(name);
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
		driver.findElementById("createLeadForm_lastName").sendKeys(""+num);
		driver.findElementByName("submitButton").click();
	}
	@DataProvider(name="paneer")
	public Object[][] fetchData(){
		Object[][] data=new Object[2][3];
		data[0][0]="testleaf";
		data[0][1]="karthi";
		data[0][2]=0102;

		data[1][0]="testleaf";
		data[1][1]="paneer";
		data[1][2]=0103;
		return data;




	}


}
