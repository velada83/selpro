package week3day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnAction {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://jqueryui.com/draggable/");
		driver.get("https://jqueryui.com/droppable/");
		driver.get("https://jqueryui.com/selectable/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().frame(0);
		WebElement draggable =driver.findElementById("draggable");
		WebElement droppable =driver.findElementById("droppable");
		WebElement selectable =driver.findElementById("selectable");
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(draggable, 150, 65).perform();
		builder.dragAndDropBy(droppable, 150, 65).perform();
		WebElement item1 = driver.findElementByXPath("//li[text()=Item 1");
		WebElement item3 = driver.findElementByXPath("//li[text()=Item 3");
		WebElement item5 = driver.findElementByXPath("//li[text()=Item 5");
		

	}

}
