package reports;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class HtmlReports {
@Test
	public void reports() throws IOException {
		ExtentHtmlReporter html = new ExtentHtmlReporter("\\Vela\\Automation\\Selenium\\SelOct2018\\reports");
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("Login","Login to reports");
		test.assignCategory("Vela");
		test.assignCategory("Smoke");
		test.pass("uname entered",MediaEntityBuilder.createScreenCaptureFromPath(".././snaps/dashboard.png").build());
		
		test.fail("uname not entered",MediaEntityBuilder.createScreenCaptureFromPath(".././snaps/dashboard.png").build());
		extent.flush();
		
		
		

		

	}

}
