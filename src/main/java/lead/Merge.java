package lead;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
@Test
public class Merge {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		Thread.sleep(2000);
		driver.findElementByLinkText("Merge Leads").click();
		Thread.sleep(1000);
		driver.findElementByXPath("((//input[@class='XdijitInputField dijitInputFieldValidationNormal'])[1])").sendKeys("10083");
		driver.findElementByXPath("((//input[@class='XdijitInputField dijitInputFieldValidationNormal'])[2])").sendKeys("10123");
		driver.findElementByXPath("(//a[@class='buttonDangerous'])").click();
		Alert Merge=driver.switchTo().alert();
		Merge.accept();
		Thread.sleep(1000);
		driver.findElementByXPath("(//a[text()='Find Leads'])").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//input[@name='id']").sendKeys("10083");
		driver.findElementByXPath("(//button[text()='Find Leads'])").click();
		Thread.sleep(2000);
		String text = driver.findElementByXPath("(//div[@class='x-paging-info'])").getText();
		if (text.equals("No records to display")) {
			System.out.println("matched");
		}
		else {
			System.out.println("not matched");

		}
	}
}




