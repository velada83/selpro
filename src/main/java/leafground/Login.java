package leafground;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Login {

	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	//launch the browser
	ChromeDriver driver = new ChromeDriver();
	//load url
	driver.get("http://leafground.com/pages/Alert.html");
	//maximize the browser
	driver.manage().window().maximize();
	//find alertbox
   // driver.findElementByXPath("//button[text()='Alert Box']").click();
    
    //find prompt box
	driver.findElementByXPath("//button[text()='Prompt Box']").click();
	 Alert alert = driver.switchTo().alert(); 
	alert.sendKeys("vela");
	String text=alert.getText();
	System.out.println(text);
	alert.sendKeys("vela");
    alert.accept();
    
		}

}
